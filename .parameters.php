<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

CModule::IncludeModule('fileman');
CMedialib::Init();

if (!function_exists('oddBuildCollectionsList')) {
	function oddBuildCollectionsList($Collections, $arColTree, $level, &$list) {
		if ($Collections === false && $arColTree === false) {
			$res = CMedialib::GetCollectionTree();
			$Collections = $res['Collections'];
			$arColTree = $res['arColTree'];
		}

		for ($i = 0, $l = count($arColTree); $i < $l; $i++) {
			$col = $Collections[$arColTree[$i]['id']];
			if (!is_array($col))
				continue;
			$levelHtml = str_repeat(' . ', $level);
			$list[$arColTree[$i]['id']] = $levelHtml . '[' . $arColTree[$i]['id'] . '] ' . $col['NAME'];

			if (count($arColTree[$i]['child'])) {
				oddBuildCollectionsList($Collections, $arColTree[$i]['child'], $level + 1, $list);
			}
		}
	}
}

$arMediaLib = array();
oddBuildCollectionsList(false, false, 0, $arMediaLib);

$arComponentParameters = array(
	'PARAMETERS' => array(
		'INCLUDE_JQUERY' => array(
			'PARENT' => 'BASE',
			'NAME' => 'Подключить jQuery',
			'TYPE' => 'CHECKBOX',
			'DEFAULT' => 'N',
		),
		'INCLUDE_FANCYBOX' => array(
			'PARENT' => 'BASE',
			'NAME' => 'Подключить Fancybox 2',
			'TYPE' => 'CHECKBOX',
			'DEFAULT' => 'N',
		),
		'MEDIA_ID' => array(
			'PARENT' => 'BASE',
			'NAME' => 'Коллекции медиабиблиотеки',
			'TYPE' => 'LIST',
			'VALUES' => $arMediaLib,
			'MULTIPLE' => 'Y',
			'SIZE' => 6
		),
		'MEDIA_SORT_FIELD' => array(
			'PARENT' => 'BASE',
			'NAME' => 'Сортировать по',
			'TYPE' => 'LIST',
			'VALUES' => array(
				'' => 'ID',
				'NAME' => 'названию',
				'DESCRIPTION' => 'описанию',
			)
		),
		'MEDIA_SORT_ORDER' => array(
			'PARENT' => 'BASE',
			'NAME' => 'Порядок сортировки',
			'TYPE' => 'LIST',
			'VALUES' => array(
				'ASC' => 'по возрастанию',
				'DESC' => 'по убыванию',
			)
		),
		'PREVIEW_WIDTH' => array(
			'PARENT' => 'BASE',
			'NAME' => 'Ширина миниатюры',
			'TYPE' => 'TEXT',
			'DEFAULT' => '200'
		),
		'PREVIEW_HEIGHT' => array(
			'PARENT' => 'BASE',
			'NAME' => 'Высота миниатюры',
			'TYPE' => 'TEXT',
			'DEFAULT' => '160'
		),
		'PREVIEW_RESIZE_TYPE' => array(
			'PARENT' => 'BASE',
			'NAME' => 'Тип масштабирования миниатюры',
			'TYPE' => 'LIST',
			'VALUES' => array(
				'BX_RESIZE_IMAGE_EXACT' => 'точно по размерам',
				'BX_RESIZE_IMAGE_PROPORTIONAL' => 'с сохранением пропорций',
			)
		),
		'ROW_SIZE' => array(
			'PARENT' => 'BASE',
			'NAME' => 'Количество миниатюр на строке',
			'TYPE' => 'TEXT',
			'DEFAULT' => ''
		),
		'IMAGE_WIDTH' => array(
			'PARENT' => 'BASE',
			'NAME' => 'Максимальная ширина увеличенной картинки',
			'TYPE' => 'TEXT',
			'DEFAULT' => '1200'
		),
		'IMAGE_HEIGHT' => array(
			'PARENT' => 'BASE',
			'NAME' => 'Максимальная высота увеличенной картинки',
			'TYPE' => 'TEXT',
			'DEFAULT' => '900'
		),

		'CSS_CLASSES' => array(
			'PARENT' => 'ADDITIONAL_SETTINGS',
			'NAME' => 'Дополнительные CSS классы',
			'TYPE' => 'TEXT',
			'DEFAULT' => ''
		),
		'MAX_COUNT' => array(
			'PARENT' => 'ADDITIONAL_SETTINGS',
			'NAME' => 'Максимальное количество выводимых элементов',
			'TYPE' => 'TEXT',
			'DEFAULT' => ''
		),
		'OFFSET_COUNT' => array(
			'PARENT' => 'ADDITIONAL_SETTINGS',
			'NAME' => 'Пропустить указанное количество элементов',
			'TYPE' => 'TEXT',
			'DEFAULT' => ''
		),
		'WATERMARK' => array(
			'PARENT' => 'ADDITIONAL_SETTINGS',
			'NAME' => 'Наносить водяной знак',
			'TYPE' => 'CHECKBOX',
			'DEFAULT' => 'N',
			'REFRESH' => 'Y'
		),
		'CACHE_TIME' => array(
			'DEFAULT' => 36000000
		)
	)
);

if (isset($arCurrentValues['WATERMARK']) && $arCurrentValues['WATERMARK'] == 'Y') {
	$arComponentParameters['PARAMETERS']['WATERMARK_PATH'] = array(
		'PARENT' => 'ADDITIONAL_SETTINGS',
		'NAME' => 'Путь к картинке водяного знака (относительно корня сайта)',
		'TYPE' => 'TEXT',
		'DEFAULT' => ''
	);

	$arComponentParameters['PARAMETERS']['WATERMARK_POSITION'] = array(
		'PARENT' => 'ADDITIONAL_SETTINGS',
		'NAME' => 'Положение водяного знака',
		'TYPE' => 'LIST',
		'VALUES' => array(
			'tl' => 'Сверху слева',
			'tc' => 'Сверху по центру',
			'tr' => 'Сверху справа',
			'ml' => 'Слева',
			'mc' => 'По центру',
			'mr' => 'Справа',
			'bl' => 'Снизу слева',
			'bc' => 'Снизу по центру',
			'br' => 'Снизу справа'
		),
		'DEFAULT' => 'br'
	);
}