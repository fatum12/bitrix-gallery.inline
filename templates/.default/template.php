<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if (!empty($arResult['ELEMENTS'])):
	$totalCount = count($arResult['ELEMENTS']);
	$onRow = $arParams['ROW_SIZE'];
	$galleryId = 'inline-gallery-' . implode('_', $arParams['MEDIA_ID']);
?>
<div class="inline-gallery js-inline-gallery <?php echo $arParams['CSS_CLASSES'];?>">
	<?php foreach($arResult["ELEMENTS"] as $key => $arItem):
		$classes = array();
		if ($onRow > 0 && ($key + 1) % $onRow == 0) {
			$classes[] = 'inline-gallery-item-last';
		}
		if ($onRow > 0 && ($key + 1) % $onRow == 1) {
			$classes[] = 'inline-gallery-item-first';
		}
		$classes[] = 'inline-gallery-item-' . $key;
	?>
		<div class="inline-gallery-item <?php echo implode(' ', $classes);?>">
			<a href="<?php echo $arItem['DETAIL_PICTURE']['SRC'];?>" rel="<?php echo $galleryId;?>" title="<?=htmlspecialchars($arItem['DESCRIPTION'])?>">
				<img src="<?php echo $arItem['PREVIEW_PICTURE']['SRC'];?>" width="<?php echo $arItem['PREVIEW_PICTURE']['WIDTH'];?>" height="<?php echo $arItem['PREVIEW_PICTURE']['HEIGHT'];?>">
			</a>
		</div>
		<?php if ($onRow > 0 && $key != $totalCount - 1 && ($key + 1) % $onRow == 0):?>
			<br>
		<?php endif;?>
	<?php endforeach;?>
</div>
<?php endif;?>
