<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if (!empty($arResult['ELEMENTS'])):
	// группируем элементы по коллекциям
	$groups = array();
	foreach ($arResult['ELEMENTS'] as $arItem) {
		if (!is_array($groups[$arItem['COLLECTION_ID']])) {
			$groups[$arItem['COLLECTION_ID']] = array();
		}
		$groups[$arItem['COLLECTION_ID']][] = $arItem;
	}

	$totalCount = count($groups);
	$onRow = $arParams['ROW_SIZE'];
	$index = 0;
?>
<div class="inline-gallery js-inline-gallery <?php echo $arParams['CSS_CLASSES'];?>">
	<?php foreach ($groups as $groupId => $elements):
		$galleryId = 'inline-gallery-group-' . $groupId;
		$classes = array();
		if ($onRow > 0 && ($index + 1) % $onRow == 0) {
			$classes[] = 'inline-gallery-item-last';
		}
		if ($onRow > 0 && ($index + 1) % $onRow == 1) {
			$classes[] = 'inline-gallery-item-first';
		}
		$classes[] = 'inline-gallery-item-' . $index;
	?>
	<div class="inline-gallery-item <?php echo implode(' ', $classes);?>">
		<?php foreach ($elements as $key => $arItem):?>
			<?php
			// показываем первый элемент
			if ($key == 0):?>
				<a href="<?php echo $arItem['DETAIL_PICTURE']['SRC'];?>" rel="<?php echo $galleryId;?>" title="<?=htmlspecialchars($arItem['DESCRIPTION'])?>">
					<img src="<?php echo $arItem['PREVIEW_PICTURE']['SRC'];?>" width="<?php echo $arItem['PREVIEW_PICTURE']['WIDTH'];?>" height="<?php echo $arItem['PREVIEW_PICTURE']['HEIGHT'];?>">
				</a>
			<?php else:?>
				<a style="display: none;" href="<?php echo $arItem['DETAIL_PICTURE']['SRC'];?>" rel="<?php echo $galleryId;?>" title="<?=htmlspecialchars($arItem['DESCRIPTION'])?>"></a>
			<?php endif;?>
		<?php endforeach;?>
	</div>
	<?php if ($onRow > 0 && $index != $totalCount - 1 && ($index + 1) % $onRow == 0):?>
		<br>
	<?php endif;?>
	<?php
		$index++;
	endforeach;?>
</div>
<?php endif;?>
