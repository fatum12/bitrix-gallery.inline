# Простая галерея изображений для 1С-Битрикс #

Компонент для CMS [1С-Битрикс](http://www.1c-bitrix.ru), позволяющий выводить на странице коллекции медиабиблиотеки в 
виде галереи миниатюр с увеличением по клику. Всплывающее окно реализовано с помощью jQuery плагина [fancyBox 2](http://fancyapps.com/fancybox/).

## Установка ##

Скопировать файлы компонента в папку `/bitrix/components/<ваше_пространство_имён>/gallery.inline` и обновить кеш 
визуального редактора. После этого в дереве компонентов редактора в папке `OddLabs` появится компонент
**Галерея изображений**.

![Screenshot 1](http://odd.su/projects/bitrix-gallery.inline/screen_1.png)

![Screenshot 2](http://odd.su/projects/bitrix-gallery.inline/screen_2.png)

![Screenshot 3](http://odd.su/projects/bitrix-gallery.inline/screen_3.png)