<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

if ($arParams['INCLUDE_JQUERY'] == 'Y') {
	$APPLICATION->AddHeadScript($componentPath . '/js/jquery-1.11.1.min.js');
}
if ($arParams['INCLUDE_FANCYBOX'] == 'Y') {
	$APPLICATION->SetAdditionalCSS($componentPath . '/js/fancybox/jquery.fancybox.css');
	$APPLICATION->AddHeadScript($componentPath . '/js/fancybox/jquery.fancybox.pack.js');
}
$APPLICATION->AddHeadScript($componentPath . '/js/fancybox.activator.js');

$arParams['ROW_SIZE'] = intval($arParams['ROW_SIZE']);

// кеширование
if ($this->StartResultCache()) {
	CModule::IncludeModule('fileman');
	CMedialib::Init();
	// коллекции
	$collections = CMedialibCollection::GetList(array('arFilter' => array('ID' => $arParams['MEDIA_ID'])));
	$arResult['COLLECTIONS'] = array();
	foreach ($collections as $collection) {
		$arResult['COLLECTIONS'][$collection['ID']] = $collection;
	}
	// элементы
	$arElements = CMedialibItem::GetList(array(
		'arCollections' => $arParams['MEDIA_ID']
	));

	// сортировка
	if ($arParams['MEDIA_SORT_FIELD'] != '') {
		$sort = array();
		foreach ($arElements as $arElement) {
			if (!empty($arElement[$arParams['MEDIA_SORT_FIELD']])) {
				$sort[$arElement[$arParams['MEDIA_SORT_FIELD']]] = $arElement;
			} else {
				$sort[] = $arElement;
			}
		}
		ksort($sort);
		$arElements = $sort;
	}
	if ($arParams['MEDIA_SORT_ORDER'] == 'DESC') {
		$arElements = array_reverse($arElements);
	}

	if (isset($arParams['OFFSET_COUNT']) && $arParams['OFFSET_COUNT'] != '') {
		$arParams['OFFSET_COUNT'] = intval($arParams['OFFSET_COUNT']);
		if ($arParams['OFFSET_COUNT'] > 0) {
			$arElements = array_slice($arElements, $arParams['OFFSET_COUNT']);
		}
	}

	if (isset($arParams['MAX_COUNT']) && $arParams['MAX_COUNT'] > 0) {
		$arElements = array_slice($arElements, 0, $arParams['MAX_COUNT']);
	}

	foreach ($arElements as $arElement) {
		if (isset($arParams['WATERMARK']) && $arParams['WATERMARK'] == 'Y' &&
			isset($arParams['WATERMARK_PATH']) && $arParams['WATERMARK_PATH'] !== '') {

			$arWaterMark = array(
				array(
					"name" => "watermark",
					"position" => isset($arParams['WATERMARK_POSITION']) ? $arParams['WATERMARK_POSITION'] : 'br',
					"type" => "image",
					"size" => "real",
					"file" => $_SERVER["DOCUMENT_ROOT"] . $arParams['WATERMARK_PATH'], // Путь к картинке
					"fill" => "exact",
				)
			);
		}
		else {
			$arWaterMark = false;
		}
		$thumb = CFile::ResizeImageGet(
			$arElement['SOURCE_ID'],
			array(
				'width' => $arParams['PREVIEW_WIDTH'],
				'height' => $arParams['PREVIEW_HEIGHT']
			),
			constant($arParams['PREVIEW_RESIZE_TYPE']),
			true
		);
		$image = CFile::ResizeImageGet(
			$arElement['SOURCE_ID'],
			array(
				'width' => $arParams['IMAGE_WIDTH'],
				'height' => $arParams['IMAGE_HEIGHT']
			),
			BX_RESIZE_IMAGE_PROPORTIONAL,
			true,
			$arWaterMark
		);
		$arResult['ELEMENTS'][] = array(
			'PREVIEW_PICTURE' => array(
				'SRC' => $thumb['src'],
				'WIDTH' => $thumb['width'],
				'HEIGHT' => $thumb['height']
			),
			'DETAIL_PICTURE' => array(
				'SRC' => $image['src'],
				'WIDTH' => $image['width'],
				'HEIGHT' => $image['height']
			),
			'NAME' => $arElement['NAME'],
			'DESCRIPTION' => $arElement['DESCRIPTION'],
			'COLLECTION_ID' => $arElement['COLLECTION_ID']
		);
	}

	$this->IncludeComponentTemplate();
}