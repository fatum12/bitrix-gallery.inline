<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$arComponentDescription = array(
	'NAME' => 'Галерея изображений',
	'DESCRIPTION' => 'Галерея изображений',
	'ICON' => '/images/icon.gif',
	'PATH' => array(
		'ID' => 'oddlabs',
		'NAME' => 'OddLabs'
	),
	'CACHE_PATH' => 'Y'
);
