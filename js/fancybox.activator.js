if(window.jQuery) {
	(function($) {

		$(document).ready(function() {
			if ($().fancybox) {
				$('.js-inline-gallery a').fancybox({
					closeBtn: true,
					loop: false,
					//openEffect: 'none',
					//closeEffect: 'none',
					//nextEffect: 'none',
					//prevEffect: 'none',
					tpl: {
						closeBtn : '<a title="Закрыть" class="fancybox-item fancybox-close" href="javascript:;"></a>',
						next     : '<a title="Следующее фото" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>',
						prev     : '<a title="Предыдущее фото" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>'
					},
					helpers: {
						overlay: {
							locked: false
						}
					}
				});
			}
		});

	})(jQuery);
}